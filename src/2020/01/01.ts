import * as fs from 'fs';

const data: string = fs.readFileSync(`${__dirname}/data.txt`, "utf8");
const parsedData =  data.split('\n').map(Number);
const target = 2020;

parsedData.find((thisNumber, index) => {
  const found = parsedData.slice(index + 1).find(otherNumber => (thisNumber + otherNumber) === target);
  console.log("run:",  index);
  if(found) {
    console.log(thisNumber, found, thisNumber * found)
  }
  return found;
});


