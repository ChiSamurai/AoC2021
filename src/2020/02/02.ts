import * as fs from 'fs';

const data: string = fs.readFileSync(`${__dirname}/data.txt`, 'utf8');
const parsedData = data.split('\n');

const result = parsedData.filter((line, index) => {
  const splittedLine = line.split(' ');
  const minMax = splittedLine[0].split('-').map(Number);
  const searchChar = splittedLine[1].charAt(0);
  const password = splittedLine[2];
  const regEx = new RegExp(searchChar, 'g');
  const occurrences = password.match(regEx)?.length;
  return occurrences && !(occurrences < minMax[0] || occurrences > minMax[1]);

});

console.log(result.length);


