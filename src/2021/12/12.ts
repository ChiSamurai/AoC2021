import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = true;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

type Cave = string;
type Route = Cave[];

interface IPath {
    from: Cave,
    to: Cave
}

const paths: IPath[] = dataRows.map(dataRow => dataRow.split('-')).map(([from, to])=> {
    return {
        from
        , to
    } as IPath
});

function isSmallCave(cave: Cave) {
    return cave.toLowerCase() === cave;
}

console.table(paths)


function findNextCaveOptions(partialRoute: Route, thisCave: Cave): Cave[] {
    return [
        ...paths.filter(path => path.from === thisCave).map(path => path.to),
        ...paths.filter(path => path.to === thisCave).map(path => path.from)
        ].filter(nextCave => (!isSmallCave(nextCave) || !partialRoute.includes(nextCave)));
}

function addNextStops(partialRoute: Route, target: Cave): Route[] {
    const thisCave = partialRoute[partialRoute.length -1];
    const nextOptions = findNextCaveOptions(partialRoute, thisCave);
    if(nextOptions.length === 0) return [partialRoute];

    const nextRoutes: Route[] = [];
    nextOptions.forEach(nextOption => {
        if(nextOption === target) {
            nextRoutes.push([...partialRoute, nextOption]);
        } else {
            nextRoutes.push(...addNextStops([...partialRoute, nextOption], target));
        }
    })
    return nextRoutes;
}


//find the starts
const routes: Route[] = addNextStops(['start'], 'end');

const fullRoutes = routes.filter(route => route[route.length -1] === 'end');

const fullRoutesWithSmallCaves = fullRoutes.filter(route => route.find(cave => isSmallCave(cave)));
console.log("fullRoutes", fullRoutes.length, "fullRoutesWithSmallCaves", fullRoutesWithSmallCaves.length);


/*const test = startPaths.map(startPath => findNextCaveOptions(startPath.to, []));*/

