import * as fs from 'fs';
import _ from 'lodash';

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

type Cave = string;
type Route = string;

interface IPath {
  from: Cave,
  to: Cave
}

const paths: IPath[] = dataRows.map(dataRow => dataRow.split('-')).map(([from, to]) => {
  return {
    from
    , to
  } as IPath;
});

function isSmallCave(cave: Cave) {
  return cave.toLowerCase() === cave;
}

function findNextCaveOptions(partialRoute: Route, thisCave: Cave): Cave[] {
  const partialRouteHops = partialRoute.split(',');

  return [
    ...paths.filter(path => path.from === thisCave).map(path => path.to),
    ...paths.filter(path => path.to === thisCave).map(path => path.from)
  ].filter(nextCave => {
    if (!isSmallCave(nextCave)) {
      return true;
    }

    // was not yet visited => valid?
    if (!partialRouteHops.includes(nextCave)) {
      return true;
    }

    //don't allow 'start'
    if (nextCave === 'start') {
      return false;
    }

    const visitedSmallCaves: Cave[] = partialRouteHops.filter(cave => isSmallCave(cave));
    const uniqueCaves = _.uniq(visitedSmallCaves);
    const twoTimesVisitedCaveFound = uniqueCaves.map(cave => visitedSmallCaves.filter(visited => visited === cave).length).find(count => count > 1);
    return !twoTimesVisitedCaveFound;
  });
}

function addNextStops(partialRoute: Route, target: Cave): Route[] {
  const partialRouteHops = partialRoute.split(',');
  const thisCave = partialRouteHops[partialRouteHops.length - 1];
  const nextOptions = findNextCaveOptions(partialRoute, thisCave);

  const nextRoutes: Route[] = [];
  if (nextOptions.length !== 0) {
    nextOptions.forEach(nextOption => {
      if (nextOption === target) {
        nextRoutes.push([partialRoute, nextOption].join(','));
      } else {
        nextRoutes.push(...addNextStops([partialRoute, nextOption].join(','), target));
      }
    });
    return nextRoutes;
  } else {
    return nextRoutes;
  }
}


//find the starts
let routes: Route[] = addNextStops('start', 'end');

console.log('fullRoutes', routes.length);

