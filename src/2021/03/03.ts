import * as fs from 'fs';

function analyzeBitArray(bitArray: Bit[]): [Bit, Bit]{
    let gammaValue: Bit;
    let epsilonValue: Bit;

    const countTrue = bitArray.filter(bit => bit === 1).length;
    if (countTrue == bitArray.length / 2) {
        throw "even set and unset bits => solution undefined";
    }
    gammaValue = (countTrue < bitArray.length / 2) ? 0 : 1;
    epsilonValue = gammaValue === 1 ? 0 : 1;
    return [gammaValue, epsilonValue];
}

const data: string = fs.readFileSync(`${__dirname}/data.txt`, 'utf8');
const parsedData = data.split('\n').map(entry => entry.trim());

const gamma: Word = [];
const epsilon: Word = [];

type Bit = 0 | 1;
type Word = Bit[];

const columnWords: Word[] = [];

parsedData.forEach((thisValue: string, index) => {
    const thisInputWord = thisValue.split('');
    for (let i = 0; i < thisInputWord.length; i++) {
        if (columnWords[i] === undefined) {
            columnWords[i] = [];
        }
        columnWords[i].push(parseInt(thisInputWord[i], 10) as Bit);
    }
});

const bitsPerColumnRow = columnWords.length;

try {
    for (let i = 0; i < bitsPerColumnRow; i++) {
        const [gammaValue, epsilonValue] = analyzeBitArray(columnWords[i]);
        gamma.push(gammaValue);
        epsilon.push(epsilonValue);
    }
} catch (error) {
    console.error(error);
}

const gammaDecimal = parseInt(gamma.join(''), 2);
const epsilonDecimal = parseInt(epsilon.join(''), 2);

console.log('gamma decimal:', gammaDecimal);
console.log('epsilon binary:', epsilonDecimal);
console.log('solution:', (gammaDecimal * epsilonDecimal));
