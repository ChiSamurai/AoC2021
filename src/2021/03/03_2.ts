import * as fs from 'fs';
const EXAMPLE_DATA_SOURCE = false;

type Bit = 0 | 1;
type Word = Bit[];

const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

function getMostCommonBit(bitArray: Bit[]): Bit {
    let mostCommon: Bit;

    const countTrue = bitArray.filter(bit => bit === 1).length;
    mostCommon = (countTrue < bitArray.length / 2) ? 0 : 1;
    return mostCommon;
}


function getColumnWord(dataWords: Word[], onlyColumn: number): Word {
    return dataWords.map(dataWord => dataWord[onlyColumn]);
}

function filterWordsByPositionBit(dataWords: Word[], value: Bit, position: number): Word[] {
    return dataWords.filter(thisWord => thisWord[position] === value);
}

function findRate(words: Word[], rate: 'oxygenGen' | 'co2Scrubber'): Word {
    let col = 0;
    const maxCol = words[0].length;
    do {
        const columnWord: Word = getColumnWord(words, col);
        const columnMostCommonBit = getMostCommonBit(columnWord);
        const findBit = rate === "oxygenGen" ? columnMostCommonBit : columnMostCommonBit === 1 ? 0 : 1;
        words = filterWordsByPositionBit(words, findBit ? 1 : 0, col);
        col++;
    } while (col < maxCol && words.length > 1)
    return words[0];
}

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataWords: Word[] = data.split('\n').map(entry => {
    return entry.trim().split('').map(stringArray => parseInt(stringArray, 10)) as Word
});

const oxygenRate =parseInt(findRate(dataWords, "oxygenGen").join(''), 2)
const co2ScrubberRate = parseInt(findRate(dataWords, 'co2Scrubber').join(''), 2);

console.log("oxygen generator value", oxygenRate);
console.log("CO2 scrubber rating", co2ScrubberRate);

console.log("Solution", co2ScrubberRate * oxygenRate)
