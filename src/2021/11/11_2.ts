import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

const dataMatrix: number[][] = dataRows.map(dataRow => dataRow.split('').map(Number));


interface ICoord {
  x: number,
  y: number
}


function cloneMatrix(dataMatrix: number[][]): number[][] {
  const newMatrix: number[][] = [];
  dataMatrix.forEach(row => newMatrix.push(row.slice()));
  return newMatrix;
}

/*
function output(dataMatrix: number[][], flash = false) {
    for (let y = 0; y < dataMatrix[0].length; y++) {
        let line = '';
        for (let x = 0; x < dataMatrix.length; x++) {
            const value = dataMatrix[y][x];
            line += value === 0 ? '*' : value.toString()
        }
        flash ? console.log('\x1b[36m%s\x1b[0m', line) : console.log(line);
    }
    console.log('\n');
}
*/

function increaseEneryLevel(dataMatrix: number[][]) {
  for (let y = 0; y < dataMatrix.length; y++) {
    for (let x = 0; x < dataMatrix[y].length; x++) {
      dataMatrix[x][y]++;
    }
  }
}

function increaseAdjacentEneryLevel(coord: ICoord, dataMatrix: number[][]): void {
  for (let y = coord.y - 1; y <= coord.y + 1; y++) {
    for (let x = coord.x - 1; x <= coord.x + 1; x++) {
      if (!(y === coord.y && x === coord.x) && y >= 0 && x >= 0 && y < dataMatrix.length && x < dataMatrix[y].length) {
        dataMatrix[y][x]++;
      }
    }
  }
}

function flash(flashCoords: ICoord[], flashedMatrix:boolean[][]): void {
  flashCoords.forEach(flashCoord => {
    flashes++;
    increaseAdjacentEneryLevel(flashCoord, dataMatrix);
    flashedMatrix[flashCoord.y][flashCoord.x] = true;
  })
}

function findFlashReady(dataMatrix: number[][], flashedMatrix: boolean[][]): ICoord[] {
  const flashReadyCoords: ICoord[] = [];
  for (let y = 0; y < dataMatrix.length; y++) {
    for (let x = 0; x < dataMatrix[y].length; x++) {
      if (!flashedMatrix[y][x] && dataMatrix[y][x] > 9) {
        flashReadyCoords.push({x, y});
      }
    }
  }
  return flashReadyCoords;
}


let flashedMatrix: boolean[][] = [];
let flashes = 0;

function resetEnergy(dataMatrix: number[][]) {
  for (let y = 0; y < dataMatrix.length; y++) {
    for (let x = 0; x < dataMatrix[y].length; x++) {
      if (dataMatrix[y][x] > 9) {
        dataMatrix[y][x] = 0;
      }
    }
  }
}

function isFullyFlashed(flashedMatrix: boolean[][]): boolean {
  const notFlashed = flashedMatrix.reduce((acc, row) => [...acc, ...row], []).filter(flashed => !flashed);
  return (notFlashed.length === 0);
}

let fullFlashedStep: number | undefined;
let i = 0;
do {
  flashedMatrix = [];
  dataMatrix.forEach((row, index) => flashedMatrix.push(Array(dataMatrix[index].length).fill(false)));

  increaseEneryLevel(dataMatrix);
  let flashFields = findFlashReady(dataMatrix, flashedMatrix);
  do  {
    flash(flashFields, flashedMatrix);
    flashFields = findFlashReady(dataMatrix, flashedMatrix);
  } while (flashFields.length > 0);
  resetEnergy(dataMatrix);
  if(isFullyFlashed(flashedMatrix)) {
    fullFlashedStep = i;
  }
  i++;
} while (fullFlashedStep === undefined);
console.log("FullFlashes", fullFlashedStep);
console.log(flashes, "flashes in ", i, "steps");
