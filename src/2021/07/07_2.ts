import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;

const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

const state = dataRows[0].split(',').map(Number);
const crabsAtPos: number[] = [];

console.log('Initial', state);

function sortCrab(xPos: number) {
  if (crabsAtPos[xPos] === undefined) {
    crabsAtPos[xPos] = 1;
  } else {
    crabsAtPos[xPos]++;
  }
}

for (let i = 0; i < state.length; i++) {
  sortCrab(state[i]);
}


const fuelNeedsToPos: number[] = Array(crabsAtPos.length).fill(0);
for (let xPos = 0; xPos < crabsAtPos.length; xPos++) {
  crabsAtPos.forEach((crabs, crabXPos) => {

    // calculateFuelNeeds for each position
    const neededSteps = Math.abs(crabXPos - xPos);
    // kleiner Gauss
    let fuelNeedsPerCrab = (neededSteps * (neededSteps + 1)) / 2;

    fuelNeedsToPos[xPos] += crabs * fuelNeedsPerCrab;
  });
}
/* console.table(fuelNeedsToPos); */
console.log('Solution', Math.min(...fuelNeedsToPos));



