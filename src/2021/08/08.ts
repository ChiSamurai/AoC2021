import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

type DigitCode = [boolean, boolean, boolean, boolean, boolean, boolean, boolean];

interface IStringLine {
  digitStrings: string[],
  displayStrings: string[]
}

/******************** main *******************/

const lineStrings: IStringLine[] = dataRows.map(row => {
  const [digitCodesFullString, displayFullString] = row.split('|').map(thisString => thisString.trim());
  const digitStrings = digitCodesFullString.split(' ').map(thisString => thisString.trim());
  const displayStrings = displayFullString.split(' ').map(thisString => thisString.trim());
  return {
    digitStrings,
    displayStrings
  } as IStringLine;
});


const result = {
  1: 0,
  4: 0,
  7: 0,
  8: 0
};
lineStrings.forEach(lineString => {
  lineString.displayStrings.forEach(string => {
    switch (string.length) {
      case 2:
        result[1]++;
        break;
      case 3:
        result[7]++;
        break;
      case 4:
        result[4]++;
        break;
      case 7:
        result[8]++;
        break;
    }
  });
  console.log(lineString.displayStrings[0].length, lineString.displayStrings[1].length, lineString.displayStrings[2].length, lineString.displayStrings[3].length);
});

console.log(result[1] + result[4] + result[7] + result[8]);




