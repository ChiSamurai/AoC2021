import * as fs from 'fs';
import _ from 'lodash';

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

interface IStringLine {
  digitStrings: string[],
  displayStrings: string[]
}

function identifyNumbers(digitLineStrings: string[]): string[][] {
  const digitSegments = digitLineStrings.map(thisString => thisString.split(''));
  // find the identifiable digits
  const digitsIdentified: string[][] = [];
  const index1 = digitSegments.findIndex(thisString => thisString.length === 2);
  digitsIdentified[1] = digitSegments[index1];
  digitSegments.splice(index1, 1);

  const index4 = digitSegments.findIndex(thisString => thisString.length === 4);
  digitsIdentified[4] = digitSegments[index4];
  digitSegments.splice(index4, 1);

  const index7 = digitSegments.findIndex(thisString => thisString.length === 3);
  digitsIdentified[7] = digitSegments[index7];
  digitSegments.splice(index7, 1);

  const index8 = digitSegments.findIndex(thisString => thisString.length === 7);
  digitsIdentified[8] = digitSegments[index8];
  digitSegments.splice(index8, 1);


  // identify the 3
  const index3 = digitSegments.findIndex(thisString => thisString.length === 5 && _.intersection(thisString, digitsIdentified[1]).length === 2);
  digitsIdentified[3] = digitSegments[index3];
  digitSegments.splice(index3, 1);

  // identify the 9
  const index9 = digitSegments.findIndex(thisString => thisString.length === 6 && _.intersection(thisString, digitsIdentified[4]).length === 4);
  digitsIdentified[9] = digitSegments[index9];
  digitSegments.splice(index9, 1);

  // identify the 5
  const index5 = digitSegments.findIndex(thisString => thisString.length === 5 && _.intersection(thisString, digitsIdentified[9]).length === 5);
  digitsIdentified[5] = digitSegments[index5];
  digitSegments.splice(index5, 1);

  // identify the 2
  const index2 = digitSegments.findIndex(thisString => thisString.length === 5);
  digitsIdentified[2] = digitSegments[index2];
  digitSegments.splice(index2, 1);

  // identify the 0
  const index0 = digitSegments.findIndex(thisString => thisString.length === 6 && _.intersection(thisString, digitsIdentified[1]).length === 2);
  digitsIdentified[0] = digitSegments[index0];
  digitSegments.splice(index0, 1);

  // last one is the 6
  digitsIdentified[6] = digitSegments[0];
  return digitsIdentified;
}

/******************** main *******************/

const lineStrings: IStringLine[] = dataRows.map(row => {
  const [digitCodesFullString, displayFullString] = row.split('|').map(thisString => thisString.trim());
  const digitStrings = digitCodesFullString.split(' ').map(thisString => thisString.trim());
  const displayStrings = displayFullString.split(' ').map(thisString => thisString.trim());
  return {
    digitStrings,
    displayStrings
  } as IStringLine;
});
const displayNumbers: number[][] = [];
lineStrings.forEach(stringLine => {
  const identifiedNumbersSorted = identifyNumbers(stringLine.digitStrings).map(identifiedNumberCharMap => identifiedNumberCharMap.sort());
  const displayStringsCharMapsSorted = stringLine.displayStrings.map(thisString => thisString.split('').sort());

  const numbers: number[] = displayStringsCharMapsSorted.map(displayStringMap => {
    return identifiedNumbersSorted.findIndex(identifiedNumberCharMap => _.isEqual(identifiedNumberCharMap, displayStringMap));
  });
  displayNumbers.push(numbers);
});

const result: number = displayNumbers.reduce((acc, thisLineNumbers) => acc + parseInt(thisLineNumbers.join(''), 10), 0);
console.log('result', result);
