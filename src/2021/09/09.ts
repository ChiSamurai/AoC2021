import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

const dataMatrix: (Number | null)[][] = dataRows.map(dataRow => dataRow.split('').map(Number));
const resultDataMatrix: (Number | null)[][] = dataRows.map(dataRow => dataRow.split('').map(Number));

function processField(nextXY: number[], thisValue: Number | null) {
  if (dataMatrix[nextXY[0]][nextXY[1]] === thisValue) {
    return true;
  }
  // @ts-ignore
  if (dataMatrix[nextXY[0]][nextXY[1]] < thisValue) {
    processAdjacentFields(nextXY[0], nextXY[1]);
    return true;
  }
  return false;
}

function processAdjacentFields(x: number, y: number) {
  if (!dataMatrix[x][y]) return;
  const thisValue = dataMatrix[x][y];
  let hasLowerAdjacent = false;
  //process left
  if ((x - 1) > -1) {
    hasLowerAdjacent = hasLowerAdjacent || processField([(x - 1), y], thisValue);
  }
  //process right
  if ((x + 1) < dataMatrix.length) {
    hasLowerAdjacent = hasLowerAdjacent || processField([(x + 1), y], thisValue);
  }
  //process top
  if ((y - 1) > -1) {
    hasLowerAdjacent = hasLowerAdjacent || processField([x, (y - 1)], thisValue);
  }
  //process bottom
  if ((y + 1) < dataMatrix[0].length) {
    hasLowerAdjacent = hasLowerAdjacent || processField([x, (y + 1)], thisValue);
  }
  if (hasLowerAdjacent) {
    resultDataMatrix[x][y] = null;
  }
}

/*console.log(dataMatrix);*/

for (let y = 0; y < dataMatrix[0].length; y++) {
  for (let x = 0; x < dataMatrix.length; x++) {
    processAdjacentFields(x, y);
  }
}

/*console.table(resultDataMatrix);*/

let riskLevelSum = 0;
for (let x = 0; x < resultDataMatrix.length; x++) {
  for (let y = 0; y < resultDataMatrix[0].length; y++) {
    if (resultDataMatrix[x][y] !== null) {
      // @ts-ignore
      riskLevelSum += (resultDataMatrix[x][y] + 1);
      /*      console.log('[', x, ',', y, '] -> ', resultDataMatrix[x][y]);*/
    }
  }
}

console.log('Solution:', riskLevelSum);

