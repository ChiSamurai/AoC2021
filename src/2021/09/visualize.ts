import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import fullData from './dataLoader.js';
import Delaunator = require('delaunator');

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const dataRows: string[] = fullData.split('\n');

const dataMatrix: number[][] = dataRows.map(dataRow => dataRow.split('').map(Number));


const scene = new THREE.Scene();
scene.background = new THREE.Color(0xbfd1e5);

const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
camera.position.z = 5;

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const controls = new OrbitControls(camera, renderer.domElement);

/*
const geometry = new THREE.BoxGeometry();
const material = new THREE.MeshBasicMaterial({
  color: 0x000000,
  wireframe: true
});

const cube = new THREE.Mesh(geometry, material);
scene.add(cube);

*/
populateScene();

window.addEventListener('resize', onWindowResize, false);

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
  render();
}

function animate() {
  requestAnimationFrame(animate);

  controls.update();

  render();
}

function render() {
  renderer.render(scene, camera);
}

function populateScene() {
  const points = [];
  for (let y = 0; y < dataMatrix.length; y++) {
    for (let x = 0; x < dataMatrix[0].length; x++) {
      points.push(new THREE.Vector3(x / 2, y / 2, dataMatrix[y][x] / 10));
    }
  }
  const material = new THREE.PointsMaterial({color: 0x000000, size: 0.1});
  const geometry = new THREE.BufferGeometry().setFromPoints(points);
  const meshPoints = new THREE.Points(geometry, material);
  scene.add(meshPoints);


  // @ts-ignore
  const indexDelaunay = Delaunator.default.from(
    points.map(v => {
      return [v.x, v.y];
    })
  );

  const meshIndex = []; // delaunay index => three.js index
  for (let i = 0; i < indexDelaunay.triangles.length; i++) {
    meshIndex.push(indexDelaunay.triangles[i]);
  }

  geometry.setIndex(meshIndex); // add three.js index to the existing geometry
  geometry.computeVertexNormals();
  const mesh = new THREE.Mesh(
    geometry, // re-use the existing geometry
    new THREE.MeshLambertMaterial({color: 'purple', wireframe: true})
  );
  scene.add(mesh);

}

animate();
