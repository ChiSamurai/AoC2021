import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

const dataMatrix: (number)[][] = dataRows.map(dataRow => dataRow.split('').map(Number));
const resultDataMatrix: (number | null)[][] = dataRows.map(dataRow => dataRow.split('').map(Number));

function processField(nextXY: number[], thisValue: number | null) {
  if (dataMatrix[nextXY[0]][nextXY[1]] === thisValue) {
    return true;
  }
  // @ts-ignore
  if (dataMatrix[nextXY[0]][nextXY[1]] < thisValue) {
    processAdjacentFields(nextXY[0], nextXY[1]);
    return true;
  }
  return false;
}

function processAdjacentFields(x: number, y: number) {
  if (!dataMatrix[x][y]) return;
  const thisValue = dataMatrix[x][y];
  let hasLowerAdjacent = false;
  //process left
  if ((x - 1) > -1) {
    hasLowerAdjacent = hasLowerAdjacent || processField([(x - 1), y], thisValue);
  }
  //process right
  if ((x + 1) < dataMatrix.length) {
    hasLowerAdjacent = hasLowerAdjacent || processField([(x + 1), y], thisValue);
  }
  //process top
  if ((y - 1) > -1) {
    hasLowerAdjacent = hasLowerAdjacent || processField([x, (y - 1)], thisValue);
  }
  //process bottom
  if ((y + 1) < dataMatrix[0].length) {
    hasLowerAdjacent = hasLowerAdjacent || processField([x, (y + 1)], thisValue);
  }
  if (hasLowerAdjacent) {
    resultDataMatrix[x][y] = null;
  }
}

for (let y = 0; y < dataMatrix[0].length; y++) {
  for (let x = 0; x < dataMatrix.length; x++) {
    processAdjacentFields(x, y);
  }
}

interface ICoord {
  x: number,
  y: number
}

interface ICoordDepth extends ICoord {
  depth: number;
}

type Basin = ICoordDepth[];

const basinLowPointCoordinates: ICoordDepth[] = [];

for (let x = 0; x < resultDataMatrix.length; x++) {
  for (let y = 0; y < resultDataMatrix[0].length; y++) {
    if (resultDataMatrix[x][y] !== null) {
      // @ts-ignore
      basinLowPointCoordinates.push({x, y});
    }
  }
}

function isBasinField(field: ICoord, thisValue: number): boolean {
  if (dataMatrix[field.x][field.y] === 9) return false;
  return dataMatrix[field.x][field.y] > thisValue;
}

function inBasin(field: ICoord, basin: Basin): boolean {
  return !!basin.find(basinField => (basinField.x === field.x) && (basinField.y === field.y));
}


function findBasinFields(thisBasinCoord: ICoord, thisBasin: Basin) {
  const thisValue = dataMatrix[thisBasinCoord.x][thisBasinCoord.y];

  //process top
  const upCoord: ICoord = {x: thisBasinCoord.x - 1, y: thisBasinCoord.y};
  if ((upCoord.x) > -1 && !inBasin(upCoord, thisBasin) && isBasinField(upCoord, thisValue)) {
    thisBasin.push({...upCoord, depth: dataMatrix[upCoord.x][upCoord.y]});
    findBasinFields(upCoord, thisBasin);
  }
  //process down
  const downCoord: ICoord = {x: thisBasinCoord.x + 1, y: thisBasinCoord.y};
  if ((downCoord.x) < dataMatrix.length && !inBasin(downCoord, thisBasin) && isBasinField(downCoord, thisValue)) {
    thisBasin.push({...downCoord, depth: dataMatrix[downCoord.x][downCoord.y]});
    findBasinFields(downCoord, thisBasin);
  }
  //process left
  const leftCoord: ICoord = {x: thisBasinCoord.x, y: thisBasinCoord.y - 1};
  if (leftCoord.y > -1 && !inBasin(leftCoord, thisBasin) && isBasinField(leftCoord, thisValue)) {
    thisBasin.push({...leftCoord, depth: dataMatrix[leftCoord.x][leftCoord.y]});
    findBasinFields(leftCoord, thisBasin);
  }
  //process right
  const rightCoord: ICoord = {x: thisBasinCoord.x, y: thisBasinCoord.y + 1};
  if (rightCoord.y < dataMatrix[0].length && !inBasin(rightCoord, thisBasin) && isBasinField(rightCoord, thisValue)) {
    thisBasin.push({...rightCoord, depth: dataMatrix[rightCoord.x][rightCoord.y]});
    findBasinFields(rightCoord, thisBasin);
  }
}


const basins: Basin[] = [];

basinLowPointCoordinates.forEach((baseCoord: ICoord) => {
  const thisBasin: Basin = [{...baseCoord, depth: dataMatrix[baseCoord.x][baseCoord.y]}];
  findBasinFields(baseCoord, thisBasin);
  basins.push(thisBasin);
});

const basinSizes: number[] = basins.map(basin => basin.length);

const sorted = basinSizes.sort((a, b) => (b - a));
console.log('Solution', sorted[0] * sorted[1] * sorted[2]);

