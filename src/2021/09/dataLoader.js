import fs from "fs";

const fullData = fs.readFileSync(__dirname + "/data.txt", "utf8");

export default fullData;