import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;

const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');
let startData = dataRows[0].split(',').map(Number);

const daysToDo = 256;

let countings: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0];

// fill in the initial values
startData.forEach(thisNumber => countings[thisNumber]++);

console.log("initial", countings);

function progressDay() {
  const newCountings = [...countings];
  countings.forEach((value, index) => {
    if (index === 0) {
      newCountings[8] += value;
      newCountings[6] += value;
    } else {
      newCountings[index - 1] += value;
    }
    newCountings[index] -= value;
  });
  countings = [...newCountings];
}

for (let i = 0; i < daysToDo; i++) {
  progressDay();
  console.log('progressed to day', i, ':', countings.reduce((acc, thisValue) => acc += thisValue, 0));
}