import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;

const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

const state = dataRows[0].split(',').map(Number);

console.log('Initial', state);

function progressDay() {
  const affectedEntries = state.length;
  for (let i = 0; i < affectedEntries; i++) {
    if (state[i] === 0) {
      state[i] = 6;
      state.push(8);
    } else {
      state[i]--;
    }
  }
}

const daysToDo = 80;

for (let i = 0; i < daysToDo; i++) {
  progressDay();
  console.log('-- after day ', i, ':', state, state.length);
}

console.log('Final after', daysToDo, 'days', state, state.length);



