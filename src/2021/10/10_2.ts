import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

const OPENING_TAGS: string[] = ['(', '[', '{', '<'];
const CLOSING_TAGS: string[] = [')', ']', '}', '>'];

const scoreMap: Map<string, number> = new Map([
  [')', 1],
  [']', 2],
  ['}', 3],
  ['>', 4]
]);

interface IBaseError {
  message: 'corrupted' | 'incomplete';
}

interface ICorruptedError extends IBaseError {
  message: 'corrupted',
  char: ')' | ']' | '}' | '>'
}

interface IIncompleteLine extends IBaseError {
  message: 'incomplete',
  line: string,
  completion: string
}

function processLine(line: string) {
  const fullLine = line;
  const openings: string[] = [];
  do {
    const char = line[0];
    line = line.slice(1);
    if (OPENING_TAGS.includes(char)) {
      openings.push(char);
    } else {
      const closingIndex = CLOSING_TAGS.indexOf(char);
      const correspondingOpeningChar = OPENING_TAGS[closingIndex];
      const lastOpeningChar = openings.pop();
      if (lastOpeningChar !== correspondingOpeningChar) {
        throw {message: 'corrupted', char} as ICorruptedError;
      }
    }
  } while (line.length > 0);
  if (openings.length > 0) {
    let completion = '';
    do {
      // @ts-ignore
      completion += CLOSING_TAGS[OPENING_TAGS.indexOf(openings.pop())];
    } while (openings.length > 0);
    throw {
      message: 'incomplete',
      line: fullLine,
      completion
    } as IIncompleteLine;
  }
}

const completionStrings: string[] = [];

dataRows.forEach((row, index) => {
  try {
    processLine(row);
  } catch (e) {
    const error = e as IBaseError;
    switch (error.message) {
      case 'incomplete':
        completionStrings.push((e as IIncompleteLine).completion);
        break;
    }
  }
});

const scores: number[] = [];

completionStrings.forEach((completionString) => {
  let totalScore: number = 0;
  completionString.split('').forEach(key => {
    totalScore = totalScore * 5;
    console.log('key', key, scoreMap.get(key));
    // @ts-ignore
    totalScore += scoreMap.get(key);
  });
  scores.push(totalScore);
});

console.log('completions', completionStrings, 'scores', scores);
const sortedScores = scores.sort((a, b) => a - b);
console.log('sorted', sortedScores, 'count', sortedScores.length / 2);
console.log('result', sortedScores[Math.floor(sortedScores.length / 2)]);
