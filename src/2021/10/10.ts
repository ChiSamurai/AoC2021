import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

const OPENING_TAGS: string[] = ['(', '[', '{', '<'];
const CLOSING_TAGS: string[] = [')', ']', '}', '>'];

const scoreMap: Map<string, number> = new Map([
  [')', 3],
  [']', 57],
  ['}', 1197],
  ['>', 25137]
]);

interface ICorruptedError {
  message: string,
  char: ')' | ']' | '}' | '>'
}

function processLine(line: string) {
  const openings: string[] = [];
  do {
    const char = line[0];
    line = line.slice(1);
    if (OPENING_TAGS.includes(char)) {
      openings.push(char);
    } else {
      const closingIndex = CLOSING_TAGS.indexOf(char);
      const correspondingOpeningChar = OPENING_TAGS[closingIndex];
      const lastOpeningChar = openings.pop();
      if (lastOpeningChar !== correspondingOpeningChar) {
        throw {message: 'corrupted', char} as ICorruptedError;
      }
    }
  } while (line.length > 0);
}

const corruptedChars = {
  ')': 0,
  ']': 0,
  '}': 0,
  '>': 0
};

dataRows.forEach((row, index) => {
  try {
    processLine(row);
  } catch (e) {
    const errorMessage = e as ICorruptedError;
    corruptedChars[errorMessage.char]++;
  }
});

let result: number = 0;
Object.keys(corruptedChars).forEach(key => {
  // @ts-ignore
  result += corruptedChars[key] * scoreMap.get(key);
});

console.log('corruptedChars', corruptedChars, 'result', result);
