import * as fs from 'fs';

const data: string = fs.readFileSync(`${__dirname}/data.txt`, 'utf8');
const parsedData = data.split('\n').map(entry => entry.trim());

let x = 0;
let depth = 0;

type CommandFn = (n: number) => void;

const Commands: { [key: string]: CommandFn } = {
  forward: (n: number) => x += n,
  up: (n: number) => depth -= n,
  down: (n: number) => depth += n
};

parsedData.forEach((thisValue: string, index) => {
  const [command, value] = thisValue.split(' ');
  Commands[command](parseInt(value, 10));
});

console.log('x:', x, 'depth:', depth, 'product:', x * depth);


