import * as fs from 'fs';

const data: string = fs.readFileSync(`${__dirname}/data.txt`, 'utf8');
const parsedData = data.split('\n').map(entry => entry.trim());

let x = 0;
let depth = 0;
let aim = 0;

type CommandFn = (n: number) => void;

const Commands: { [key: string]: CommandFn } = {
  forward: (n: number) => {
    x += n;
    depth += (n * aim);
  },
  up: (n: number) => {
    aim -= n;
  },
  down: (n: number) => {
    aim += n;
  }
};

parsedData.forEach((thisValue: string, index) => {
  const [command, value] = thisValue.split(' ');
  Commands[command](parseInt(value, 10));
  console.log(command, '(', value, ')', 'x:', x, 'depth:', depth, 'aim:', aim);
});

console.log('x:', x, 'depth:', depth, 'aim:', aim, 'product:', x * depth);


