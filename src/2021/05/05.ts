import * as fs from "fs";

const EXAMPLE_DATA_SOURCE = false;

const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

interface ICoord {
    x: number,
    y: number
}

interface ILine {
    start: ICoord,
    end: ICoord
}

function coordStringToCoord(coordString: string): ICoord {
    const coordNumbers: number[] = coordString.split(',').map(Number);
    return {
        x: coordNumbers[0],
        y: coordNumbers[1]
    } as ICoord;
}

/*
function isHorizontal(line: ILine) {
    return line.start.x === line.end.x;
}
*/

function getAllLineCoords(line: ILine):ICoord[] {
    const coords: ICoord[] = [];
    const startX = Math.min(line.start.x, line.end.x);
    const endX = Math.max(line.start.x, line.end.x)

    const startY = Math.min(line.start.y, line.end.y);
    const endY = Math.max(line.start.y, line.end.y)

    for(let x = startX; x <= endX; x++) {
        for(let y = startY; y <= endY; y++){
            coords.push({
                x,
                y
            });
        }
    }
    return coords;

}

function createDiagram(lines: ILine[]): number[][] {
    const max: { x: number, y: number } = lines.reduce((previousValue, currentValue) => {
        const lineMaxX = Math.max(currentValue.end.x, currentValue.start.x);
        const lineMaxY = Math.max(currentValue.end.y, currentValue.start.y);
        return {
            x: previousValue.x < lineMaxX ? lineMaxX : previousValue.x,
            y: previousValue.y < lineMaxY ? lineMaxY : previousValue.y
        };
    }, {x: 0, y: 0});

    const diagram: number[][] = [];
    for (let i = 0; i <= max.y; i++) {
        diagram.push(Array(max.x + 1).fill(0));
    }
    return diagram;
}

function fillDiagram(lines: ILine[], diagram: number[][]) {
    lines.forEach(line => {
        const lineCoords = getAllLineCoords(line);
        lineCoords.forEach(lineCoord => {
            diagram[lineCoord.y][lineCoord.x] += 1;
        })
    })
}
/*
function normalizeLine(line: ILine): ILine {
    if (isHorizontal(line)) {
        return line.start.x > line.end.x ? {start: line.end, end: line.start} : line;
    } else {
        return line.start.y > line.end.y ? {start: line.end, end: line.start} : line;
    }
}
*/

function printDiagram(diagram: number[][]){
    for (let y = 0; y < diagram.length; y++){
        let line: string = '';
        for (let x = 0; x < diagram[y].length; x++){
            line += diagram[y][x].toString();
        }
        console.log(line);
    }
}

function calculateResult(diagram: number[][]): number {
    let result = 0;
    for (let y = 0; y < diagram.length; y++){
        let line: string = '';
        for (let x = 0; x < diagram[y].length; x++){
            const value = diagram[y][x];
            result += value > 1 ? 1: 0;
        }
    }
    return result;
}

/************************ Main *****************************/

let diagram: number[][] = [];
let lines: ILine[] = dataRows.map(row => {
    const coordsStrings = row.split('->').map(coordsString => coordsString.trim());
    const line = {
        start: coordStringToCoord(coordsStrings[0]),
        end: coordStringToCoord(coordsStrings[1])
    } as ILine;

    return line;
});

// filter out non horizontal & vertical lines

diagram = createDiagram(lines);
lines = lines.filter(line => (line.start.x === line.end.x || line.start.y === line.end.y));
fillDiagram(lines, diagram);
printDiagram(diagram);
console.log("Result", calculateResult(diagram));

