/** Note to myself: Next time use a geometry-library (like flatten-js) or at least think in vectors!!! :-) **/
import * as fs from "fs";

const EXAMPLE_DATA_SOURCE = false;

const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

interface ICoord {
    x: number,
    y: number
}

interface ILine {
    start: ICoord,
    end: ICoord
}

function coordStringToCoord(coordString: string): ICoord {
    const coordNumbers: number[] = coordString.split(',').map(Number);
    return {
        x: coordNumbers[0],
        y: coordNumbers[1]
    } as ICoord;
}

function isDiagonal(line: ILine) {
    return (line.start.x !== line.end.x && line.start.y !== line.end.y);
}

function getAllLineCoords(line: ILine): ICoord[] {
    const coords: ICoord[] = [];

    if (isDiagonal(line)) {
        const length = Math.abs(line.start.x - line.end.x);
        for (let i = 0; i <= length; i++) {
            const xCoord = line.start.x < line.end.x ? line.start.x + i : line.start.x - i;
            const yCoord = line.start.y < line.end.y ? line.start.y + i : line.start.y - i;
            coords.push({x: xCoord, y: yCoord});
        }
    } else {
        const startX = Math.min(line.start.x, line.end.x);
        const endX = Math.max(line.start.x, line.end.x);

        const startY = Math.min(line.start.y, line.end.y);
        const endY = Math.max(line.start.y, line.end.y);
        for (let x = startX; x <= endX; x++) {
            for (let y = startY; y <= endY; y++) {
                coords.push({
                    x,
                    y
                });
            }
        }
    }
    return coords;

}

function createDiagram(lines: ILine[]): number[][] {
    const max: { x: number, y: number } = lines.reduce((previousValue, currentValue) => {
        const lineMaxX = Math.max(currentValue.end.x, currentValue.start.x);
        const lineMaxY = Math.max(currentValue.end.y, currentValue.start.y);
        return {
            x: previousValue.x < lineMaxX ? lineMaxX : previousValue.x,
            y: previousValue.y < lineMaxY ? lineMaxY : previousValue.y
        };
    }, {x: 0, y: 0});

    const diagram: number[][] = [];
    for (let i = 0; i <= max.y; i++) {
        diagram.push(Array(max.x + 1).fill(0));
    }
    return diagram;
}

function fillDiagram(lines: ILine[], diagram: number[][]) {
    lines.forEach(line => {
        const lineCoords = getAllLineCoords(line);
        lineCoords.forEach(lineCoord => {
            diagram[lineCoord.y][lineCoord.x] += 1;
        });
    });
}

function printDiagram(diagram: number[][]) {
    for (let y = 0; y < diagram.length; y++) {
        let line: string = '';
        for (let x = 0; x < diagram[y].length; x++) {
            line += diagram[y][x].toString();
        }
    }
}

function calculateResult(diagram: number[][]): number {
    let result = 0;
    for (let y = 0; y < diagram.length; y++) {
        for (let x = 0; x < diagram[y].length; x++) {
            const value = diagram[y][x];
            result += value > 1 ? 1 : 0;
        }
    }
    return result;
}

/************************ Main *****************************/

let diagram: number[][] = [];
let lines: ILine[] = dataRows.map(row => {
    const coordsStrings = row.split('->').map(coordsString => coordsString.trim());
    const line = {
        start: coordStringToCoord(coordsStrings[0]),
        end: coordStringToCoord(coordsStrings[1])
    } as ILine;

    return line;
});

diagram = createDiagram(lines);
fillDiagram(lines, diagram);
printDiagram(diagram);
console.log("Result", calculateResult(diagram));

