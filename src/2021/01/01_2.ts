import * as fs from 'fs';

interface IIncreasedDecreased {
  increased: number[],
  decreasedOrEven: number[]
}

function checkIncreasedDecreased(numberArray: number[]): IIncreasedDecreased {
  const result: IIncreasedDecreased = {
    increased: [],
    decreasedOrEven: []
  };
  numberArray.forEach((thisValue, index) => {
    if (index > 0) {
      (thisValue > numberArray[index - 1]) ? result.increased.push(index) : result.decreasedOrEven.push(index);
    }
  });
  return result;
}

const data: string = fs.readFileSync(`${__dirname}/data.txt`, 'utf8');
const parsedData = data.split('\n').map(entry => parseFloat(entry.trim()));

const sums = [];

for (let i = 2; i < parsedData.length; i++) {
  const windowSum = parsedData[i - 2] + parsedData[i - 1] + parsedData[i];
  sums.push(windowSum);
}

const sumsLength = sums.length;
const parsedDataLength = parsedData.length;

const sortedValues = checkIncreasedDecreased(sums);

console.log('total sums', sums.length);
console.log('check first sum', sums[0] === parsedData[0] + parsedData[1] + parsedData[2]);
console.log('check last sum', sums[sumsLength - 1] === parsedData[parsedDataLength - 1] + parsedData[parsedDataLength - 2] + parsedData[parsedDataLength - 3]);

console.log('increased:', sortedValues.increased.length, 'decreasedOrEven:', sortedValues.decreasedOrEven.length, 'correct amount of values?', (sums.length - 1) == (sortedValues.increased.length + sortedValues.decreasedOrEven.length));


