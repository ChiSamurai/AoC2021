import * as fs from 'fs';

const data: string = fs.readFileSync(`${__dirname}/data.txt`, 'utf8');
const parsedData = data.split('\n').map(entry => parseFloat(entry.trim()));

const increased = [];
const decreasedOrEven = [];

parsedData.forEach((thisValue, index) => {
  if (index > 0) {
    (thisValue > parsedData[index - 1]) ? increased.push(index) : decreasedOrEven.push(index);
  }
});

console.log('increased:', increased.length, 'decreasedOrEven:', decreasedOrEven.length, 'correct amount of values?', (parsedData.length - 1) == (increased.length + decreasedOrEven.length));


