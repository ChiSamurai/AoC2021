import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;
import * as _ from "lodash";
import {parseInt} from "lodash";

type BingoCard = (number | null)[][];
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

const drawnNumbers: number[] = dataRows[0].split(',').map(Number);

const cardRows = dataRows.slice(1);
const cards: BingoCard[] = [];
let lastNumber: number | null = null;

function parseIntoChunks(row: string): number[] {
    const chunks: number[] = [];
    while (row.length > 0) {
        chunks.push(parseInt(row.substr(0, 2).trim(), 10));
        row = row.slice(3);
    }
    return chunks;
}

function getWinningCard(cards: BingoCard[], drawnNumbers: number[]): BingoCard | undefined {
    try {
        drawnNumbers.forEach(drawnNumber => {
            console.log("--- Checking number", drawnNumber);
            lastNumber = drawnNumber;
            for (let i = 0; i < cards.length; i++) {
                console.log("-- Checking card", i);
                cards[i] = eliminateDrawnNumber(cards[i], drawnNumber);
                if (cardHasWon(cards[i])) {
                    console.log("card has won!");
                    throw cards[i];
                }
            }
        })
    } catch (card) {
        return card;
    }

}

function eliminateDrawnNumber(card: BingoCard, drawnNumber: number): BingoCard {
    for (let i = 0; i < card.length; i++) {
        const row = card[i];
        for (let j = 0; j < row.length; j++) {
            if (card[i][j] === drawnNumber) {
                card[i][j] = null;
                return card;
            }
        }
    }
    return card;
}

function cardHasWon(card: BingoCard): boolean {
    // check the rows
    for (let i = 0; i < card.length; i++) {
        const numbers = card[i].filter(number => number !== null);
        if (numbers.length === 0) return true;
    }
    // check the columns
    for (let i = 0; i < card[0].length; i++) {
        const columnArray: (number | null)[] = [];
        card.forEach(row => columnArray.push(row[i]));
        const numbers = columnArray.filter(number => number !== null);
        if (numbers.length === 0) return true;
    }

    return false;
}

// first split the data into cards
cardRows.forEach(row => {
    if (row.length === 0) {
        cards.push([]);
    } else {
        _.last(cards)!.push(parseIntoChunks(row));
    }
});

// get the winning card
const winningCard = getWinningCard(cards, drawnNumbers);
console.log("winning", winningCard, lastNumber);

// calculate the score
let cardNumbersSum = 0;
winningCard?.forEach(row => {
    row.forEach(number => {
        if (number) {
            cardNumbersSum += number
        }
    })
});

if (lastNumber) {
    console.log("score", cardNumbersSum * lastNumber);
}


