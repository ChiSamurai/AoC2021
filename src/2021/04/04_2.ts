import * as fs from 'fs';

const EXAMPLE_DATA_SOURCE = false;
import * as _ from "lodash";
import {parseInt} from "lodash";

interface IBingoCard {
    numbers: (number | null)[][],
    won: boolean;
}

const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

const drawnNumbers: number[] = dataRows[0].split(',').map(Number);

const cardRows = dataRows.slice(1);
const cards: IBingoCard[] = [];

let lastNumber: number | null = null;

function parseIntoChunks(row: string): number[] {
    const chunks: number[] = [];
    while (row.length > 0) {
        chunks.push(parseInt(row.substr(0, 2).trim(), 10));
        row = row.slice(3);
    }
    return chunks;
}

function getLastWinningCard(cards: IBingoCard[], drawnNumbers: number[]): IBingoCard | undefined {
    try {
        drawnNumbers.forEach(drawnNumber => {
            console.log("--- Checking number", drawnNumber);
            lastNumber = drawnNumber;
            for (let i = 0; i < cards.length; i++) {
                console.log("-- Checking card", i);
                cards[i] = eliminateDrawnNumber(cards[i], drawnNumber);
                cards[i].won = cardHasWon(cards[i]);
                console.log("cards that won:", cards.filter(card => card.won).length);
                if (cards.filter(card => !card.won).length === 0) {
                    console.log("last card has won!");
                    throw cards[i];
                }
            }
        })
    } catch (card) {
        return card;
    }

}

function eliminateDrawnNumber(card: IBingoCard, drawnNumber: number): IBingoCard {
    for (let i = 0; i < card.numbers.length; i++) {
        const row = card.numbers[i];
        for (let j = 0; j < row.length; j++) {
            if (card.numbers[i][j] === drawnNumber) {
                card.numbers[i][j] = null;
                return card;
            }
        }
    }
    return card;
}

function cardHasWon(card: IBingoCard): boolean {
    // check the rows
    for (let i = 0; i < card.numbers.length; i++) {
        const numbers = card.numbers[i].filter(number => number !== null);
        if (numbers.length === 0) return true;
    }
    // check the columns
    for (let i = 0; i < card.numbers[0].length; i++) {
        const columnArray: (number | null)[] = [];
        card.numbers.forEach(row => columnArray.push(row[i]));
        const numbers = columnArray.filter(number => number !== null);
        if (numbers.length === 0) return true;
    }

    return false;
}

// first split the data into cards
cardRows.forEach(row => {
    if (row.length === 0) {
        cards.push({
            numbers: [],
            won: false
        });
    } else {
        _.last(cards)!.numbers.push(parseIntoChunks(row));
    }
});

// get the winning card
const lastWinningCard = getLastWinningCard(cards, drawnNumbers);
console.log("last winning", lastWinningCard, lastNumber);

// calculate the score
let cardNumbersSum = 0;
lastWinningCard?.numbers?.forEach(row => {
    row.forEach(number => {
        if (number) {
            cardNumbersSum += number
        }
    })
});

if (lastNumber) {
    console.log("score", cardNumbersSum * lastNumber);
}


