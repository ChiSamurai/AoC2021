import * as fs from 'fs';
import _ from 'lodash';

const EXAMPLE_DATA_SOURCE = false;
const dataFilename = EXAMPLE_DATA_SOURCE ? 'exampleData.txt' : 'data.txt';

const data: string = fs.readFileSync(`${__dirname}/${dataFilename}`, 'utf8');
const dataRows: string[] = data.split('\n');

interface ICoord {
  x: number,
  y: number
}

interface IFoldInstruction {
  axis: 'x' | 'y',
  coordValue: number
}

interface ISheetSize {
  x: number,
  y: number
}

function parseRawRows(rows: string[]): [ICoord[], IFoldInstruction[]] {
  const allCoords: ICoord[] = [];
  const allFoldInstructions: IFoldInstruction[] = [];

  let instructionsStarted = false;

  rows.forEach(row => {
    if (row === '') {
      instructionsStarted = true;
    } else {
      if (!instructionsStarted) {
        const coords: number[] = row.split(',').map(Number);
        allCoords.push({x: coords[0], y: coords[1]} as ICoord);
      } else {
        const foldInstructionString: string = row.split(' ')[2];
        const foldInstructionParts = foldInstructionString.split('=');
        const foldInstruction: IFoldInstruction = {
          axis: foldInstructionParts[0],
          coordValue: parseInt(foldInstructionParts[1], 10)
        } as IFoldInstruction;
        allFoldInstructions.push(foldInstruction);
      }
    }
  });
  return [allCoords, allFoldInstructions];
}

function doFold(coords: ICoord[], instruction: IFoldInstruction, sheetSize: ISheetSize): [ICoord[], ISheetSize] {
  console.log('--- folding:', instruction);

  if (instruction.axis === 'y') {
    // split the coords at ROW
    const topCoords = coords.filter(coord => coord.y < instruction.coordValue);
    const fullSheetBottomCoords = coords.filter(coord => coord.y > instruction.coordValue);

    const newSheetSize: ISheetSize = {
      ...sheetSize,
      y: sheetSize.y - (sheetSize.y - instruction.coordValue)
    };
    console.log('- newSheetSize:', newSheetSize);

    const foldedSheetBottomCoords = mirrorCoordsAtAxis(fullSheetBottomCoords, instruction, newSheetSize);
    const foldedCoords = _.uniqWith([...topCoords, ...foldedSheetBottomCoords], _.isEqual);
    return [foldedCoords, newSheetSize];
  } else {
    //split the coords at COLUMN
    const leftCoords = coords.filter(coord => coord.x < instruction.coordValue);
    const fullSheetRightCoords = coords.filter(coord => coord.x > instruction.coordValue);

    const newSheetSize: ISheetSize = {
      ...sheetSize,
      x: sheetSize.x - (sheetSize.x - instruction.coordValue)
    };
    console.log('- newSheetSize:', newSheetSize);

    const foldedSheetRightCoords = mirrorCoordsAtAxis(fullSheetRightCoords, instruction, newSheetSize);
    const foldedCoords = _.uniqWith([...leftCoords, ...foldedSheetRightCoords], _.isEqual);
    return [foldedCoords, newSheetSize];
  }
}

function mirrorCoordsAtAxis(coords: ICoord[], instruction: IFoldInstruction, sheetSize: ISheetSize): ICoord[] {
  const newCoords: ICoord[] = [];
  coords.forEach(coord => {
    newCoords.push({
      x: instruction.axis === 'x' ? sheetSize.x - 1 - (coord.x - sheetSize.x - 1) : coord.x,
      y: instruction.axis === 'y' ? sheetSize.y - 1 - (coord.y - sheetSize.y - 1) : coord.y
    } as ICoord);
  });
  return newCoords;
}


let [sheetCoords, foldInstructions] = parseRawRows(dataRows);


function createMatrix(coordsArray: ICoord[], sheetSize: ISheetSize): string[][] {
  const sheet: string[][] = [];
  for (let rowIdx = 0; rowIdx < sheetSize.y; rowIdx++) {
    const newRow = new Array(sheetSize.x).fill('.');
    coordsArray.filter(coord => coord.y === rowIdx).forEach(coord => {
      newRow[coord.x] = '#';
    });
    sheet.push(newRow);
  }
  return sheet;
}

let sheetSize: ISheetSize = {
  x: sheetCoords.reduce((acc, coord) => Math.max(acc, coord.x + 1), 0),
  y: sheetCoords.reduce((acc, coord) => Math.max(acc, coord.y + 1), 0)
} as ISheetSize;

EXAMPLE_DATA_SOURCE && console.table(createMatrix(sheetCoords, sheetSize));
foldInstructions.forEach((foldInstruction, index) => {
  [sheetCoords, sheetSize] = doFold(sheetCoords, foldInstruction, sheetSize);
  console.log('Dots after fold run', index, ':', sheetCoords.length);
  EXAMPLE_DATA_SOURCE && console.table(createMatrix(sheetCoords, sheetSize));
});

const finalMatrix = createMatrix(sheetCoords, sheetSize);
for (let y = 0; y < finalMatrix.length; y++) {
  console.log(finalMatrix[y].join(''));
}
