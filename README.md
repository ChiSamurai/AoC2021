# ChiSamurai's Advent of Code Solutions

Repo for my solutions for the Advent of Code (https://adventofcode.com/).

- Weapon of choice: Typescript
- `npm install`
- `npm install -g npx`
- `npm run run$day(_2)`, e.g. for 2021, Day 01:
    - `npm run run01`
    - `npm run run01_2`